import styles from './homepage.module.css';

export default function Home() {
  return (
    <div>
      <h1>Lorem</h1>
      Hello Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum
      voluptas dolores, inventore sapiente quasi quos, voluptatibus consequatur
      a ut tempora ab incidunt molestiae voluptate. Unde nam sequi nihil commodi
      eum tenetur consectetur blanditiis, magni maiores neque iusto aliquid
      error nostrum. Et laudantium assumenda, nulla temporibus eveniet id
      placeat molestias nesciunt illo incidunt officia itaque atque consequuntur
      nam doloribus dicta debitis delectus ab labore ducimus possimus! Iste esse
      facilis veniam! Ratione debitis, beatae corrupti iusto similique
      repellendus, exercitationem placeat culpa doloremque itaque iure iste
      cupiditate inventore. Totam deleniti praesentium ad similique temporibus
      fugiat sequi, dolore accusamus incidunt optio sed officiis illo a ut,
      atque quidem cum maxime, ab repudiandae doloremque! Aperiam odio quod
      distinctio, rerum corporis officiis, sapiente ex suscipit explicabo
      numquam est dolor provident veniam ipsam! Veniam, blanditiis ducimus
      perferendis obcaecati vero voluptatum nihil a placeat eligendi impedit
      temporibus provident porro tenetur, inventore perspiciatis nesciunt sint
      vel earum esse repellendus totam cumque praesentium labore! Esse maiores,
      non voluptate iure officia ab cum eius aut consequuntur impedit
      voluptatibus recusandae ullam nam, quis quae excepturi, facilis vitae
      illum libero dignissimos dicta ex molestias. Voluptate totam similique,
      dolorum numquam, accusamus velit rem assumenda repudiandae, a fugit optio.
      Quod ex pariatur nemo aspernatur maxime quam numquam. Esse officiis sint
      officia hic modi, nulla quis error nisi. Officia et neque sequi pariatur
      repellendus enim, vel exercitationem expedita voluptatibus alias
      voluptates nemo culpa eaque sunt cumque consequuntur dignissimos aut eos,
      ea minima ut. Repudiandae eligendi cum facere aperiam atque ipsam ratione,
      nisi quisquam perferendis aspernatur accusantium sunt quidem quaerat omnis
      explicabo provident quis necessitatibus debitis quos porro. Quaerat
      deleniti maiores harum ducimus laudantium consequuntur earum aut suscipit?
      Ea, perspiciatis neque. Corporis beatae ipsum, facilis ipsam reprehenderit
      commodi laudantium omnis quaerat sapiente facere fugit blanditiis nemo qui
      esse veritatis dicta. Rem aut sit reprehenderit veniam laborum placeat?
    </div>
  );
}
